<?php
/**
 */



/**
 * remove a directory
 */
function _removeDir($path) {
  // Add trailing slash to $path if one is not there
  if (substr($path, -1, 1) != "/") {
    $path .= "/";
  }
  foreach (glob($path . "*") as $file) {
    if (is_file($file) === TRUE) {
      // Remove each file in this Directory
      unlink($file);
    } else if (is_dir($file) === TRUE) {
      // If this Directory contains a Subdirectory, run this Function on it
      _removeDir($file);
    }
  }
  // Remove Directory once Files have been removed (If Exists)
  if (is_dir($path) === TRUE) {
    rmdir($path);
  }
}
/** END: remove a directory ***************************************************/



/**
 * get the status of your WoWRoster installation
 * @return TRUE if WoWRoster is properly installed, otherwise FALSE
 */
function _wowroster_get_status() {
  $result = db_query("SELECT * FROM {wowroster_status}");
  $status = db_fetch_array($result);
  return $status['wowroster_is_installed'] == FALSE ? FALSE : TRUE;
}
/** END: _wowroster_get_status ************************************************/



/**
 * Determines if a character has an uploaded profile
 * 
 * @param id the id number of the user
 * @return true or false
 */
function _wowroster_is_uploaded($id) {
  // Get the varialbes we need
  $roster_tblpref = variable_get('wowroster_roster_tableprefix', '');
  
  // Check to see if the member has uploaded their profile
	$query = 'SELECT * FROM '.$roster_tblpref.'players WHERE member_id='.$id.'';
  db_set_active('wowroster');
	$result = db_query($query);
  db_set_active('default');
	if (!db_num_rows($result)) {
    return FALSE;
	}
  else {
    return TRUE;
  }
}
/** END: _wowroster_is_uploaded ***********************************************/



/**
 * Generate a link to the users page
 *
 * @param name the name of the user
 * @param id the id number of the user
 * @return a full link to the users profile page
 */
function _wowroster_simpleroster_link($name, $id) {
  return l($name, 'wowroster/profile/'.$id.'');
}
/** END: _wowroster_simpleroster_link *****************************************/



/**
 * Get fields from drupal profiles
 *
 * @return returns an array of available profile fields
 */
function _drupal_get_profile_fields() {
  if (module_exists('profile')) {
    $query = 'SELECT title FROM {profile_fields}';
    $result = db_query($query);
    
    while ($fields = db_fetch_array($result)) {
      $profile_fields[] = $fields['title'];
    }
    return $profile_fields;
  } else {
    return FALSE;
  }
}
/** END: _drupal_get_profile_fields *******************************************/



/**
 * determine if the current drupal-user has a drupal_char-text-field
 * corresponding to a valid wow player
 *
 * @return returns a valid member_id
 */
function _wowroster_drupaluser_to_wowplayer() {
  $drupal_text_field = variable_get('drupal_char-text-field', t('Drupal-User'));
  $roster_tblpref = variable_get('wowroster_roster_tableprefix', '');

  if ($drupal_text_field = t('Drupal-User')) {
    // Drupal search field is the username
    $query = ("
      SELECT  name
      FROM    {users}
      WHERE   uid = '%s'
    ");
    $result = db_query($query, arg(1));
    $value = db_fetch_array($result);
    $drupal_value = $value['name'];
  } else {
    // Drupal search field is a profile_field
  }

  // get the wowroster member_id (if there is one)
  $query = ('
    SELECT    '.$roster_tblpref.'players.member_id
    FROM      '.$roster_tblpref.'players
    LEFT JOIN '.$roster_tblpref.'members
    ON '.$roster_tblpref.'members.member_id = '.$roster_tblpref.'players.member_id
    WHERE     '.$roster_tblpref.'players.name = "%s"
  ');
  db_set_active('wowroster');
  $result = db_query($query, $drupal_value);
  db_set_active('default');
  $member_id = db_fetch_array($result);
  return $member_id['member_id'];
}
/** END: _wowroster_drupaluser_to_wowplayer ***********************************/



/**
 * determine if the member is a main, alt or undefined char
 *
 * @param memberid MemberID
 * @return string chartype
 */
function _wowroster_player_is_chartype($memberid) {
  $roster_tblpref     = variable_get('wowroster_roster_tableprefix', '');
  $main_text_field    = variable_get('wowroster_main-text_field',   'note');
  $main_text_search   = variable_get('wowroster_main-text_search',  'Main');
  $alt_text_field     = variable_get('wowroster_alt-text_field',    'note');
  $alt_text_search    = variable_get('wowroster_alt-text_search',   'Twink');

  $query = 'SELECT '.$roster_tblpref.'members.'.$main_text_field.' AS main,
                   '.$roster_tblpref.'members.'.$alt_text_field.' AS alt
            FROM '.$roster_tblpref.'members
            WHERE '.$roster_tblpref.'members.member_id='.$memberid.'';

  db_set_active('wowroster');
	$result = db_query($query);
  db_set_active('default');

	$member = db_fetch_array($result);

  if (strstr($member['main'],$main_text_search)) {
    $chartype = "main";
  } else if (strstr($member['alt'],$alt_text_search)) {
    $chartype = "alt";
  } else {
    $chartype = "undefined";
  }
  return $chartype;
}
/** END: _wowroster_player_is_chartype ****************************************/



/**
 * get the mainchar or if chartype is undefined treat it as a mainchar
 *
 * @param memberid MemberID
 * @param chartype main/alt/undefined
 * @return return an array width the main-chars data
 */
function _wowroster_get_main($memberid, $chartype) {
  $roster_tblpref     = variable_get('wowroster_roster_tableprefix', '');
  $alt_text_field     = variable_get('wowroster_alt-text-field',    'note');

  $query = 'SELECT *
            FROM '.$roster_tblpref.'members
            LEFT JOIN '.$roster_tblpref.'players
            ON '.$roster_tblpref.'members.member_id = '.$roster_tblpref.'players.member_id ';

  // requested char is main
  if ($chartype === 'main' || $chartype === 'undefined') {
    $query .= 'WHERE '.$roster_tblpref.'members.member_id = '.$memberid.'';

  // requested char is alt
  } else if ($chartype === 'alt') {
    $query .= 
      'WHERE '.$roster_tblpref.'members.name = (
        SELECT SUBSTRING('.$roster_tblpref.'members.'.$alt_text_field.',8)
        FROM '.$roster_tblpref.'members
        WHERE '.$roster_tblpref.'members.member_id = '.$memberid.')';
  }

  db_set_active('wowroster');
	$result = db_query($query);
  db_set_active('default');

	$main = db_fetch_array($result);

  return $main;
}
/** END: _wowroster_get_main **************************************************/



/**
 * get all related chars of one mainchar
 */
function _wowroster_get_alts($main_member_name) {
  $roster_tblpref     = variable_get('wowroster_roster_tableprefix', '');
  $alt_text_field     = variable_get('wowroster_alt-text-field',    'note');

  $query = 
    'SELECT  *
     FROM    '.$roster_tblpref.'members
     LEFT JOIN '.$roster_tblpref.'players
     ON '.$roster_tblpref.'members.member_id = '.$roster_tblpref.'players.member_id 
     WHERE '.$roster_tblpref.'members.'.$alt_text_field.' LIKE "%'.$main_member_name.'%"';

  db_set_active('wowroster');
  $result = db_query($query);
  db_set_active('default');
  while ($alt = db_fetch_array($result)) {
    if ($alt['member_id'] && _wowroster_is_uploaded($alt['member_id'])) {
      $alts[] = $alt;
    }
  }

  return $alts;
}
/** END: _wowroster_get_alts **************************************************/
?>
