


MODULE
------
wowroster (WoWRoster 1.7.1 integration for Drupal-4.7.4)



DESCRIPTION/FEATURES
--------------------
Displays information from the wowroster database.



REQUIREMENTS
------------
Drupal 4.7.4 (MySQL >= 4.1)
TODO: wowroster for pgsql



INSTALL/CONFIG
--------------
1. Move this folder into your modules directory like you would any other module.

2. Set your $db_url in settings.php like this:
   $db_url['default']   = mysql://user:pass@host/dbname
   $db_url['wowroster'] = mysql://user:pass@host/dbname

3. Download the full_package version of WoWRoster from
   www.wowroster.net

4. Unpack WoWRoster in path/to/your/modules/wowroster/roster and make
   sure that it is writable by your webserver.

5. Enable wowroster module from administer >> modules.

6. Go to 'admin/wowroster/installer'.

7. Follow the normal Roster Installation steps within the IFrame.
   At the step where it points you to "Remove Install Files", click on the
   "finish WoWRoster Installation" button (located above the IFrame). The
   script will try to remove those files automatically.

8. The wowroster module is available now.



LICENSE
-------
GNU GENERAL PUBLIC LICENSE (GPL)
